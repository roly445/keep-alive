﻿namespace KeepAlive.Task
{
    using System.Linq;
    using Domain.Entities;
    using HtmlAgilityPack;
    using Service;

    public class TaskRunner
    {
        private static WebsiteService _websiteService;

        public TaskRunner(WebsiteService websiteService)
        {
            _websiteService = websiteService;
        }

        #region Methods

        public static void Run()
        {
            IQueryable<Website> websites = _websiteService.SelectAll();
            HtmlWeb htmlWeb = new HtmlWeb();
            foreach (Website website in websites)
            {
                HtmlDocument doc = htmlWeb.Load(website.Url);
            }
        }

        #endregion
    }
}