﻿namespace KeepAlive.Domain.Repositories
{
    using System;
    using System.Data.Entity;

    public interface IRepositoryProvider
	{
		DbContext DbContext { get; set; }

		#region Methods

		IRepository<T> GetRepositoryForEntityType<T>() where T : class;
		T GetRepository<T>(Func<DbContext, object> factory = null) where T : class;

		#endregion
	}
}