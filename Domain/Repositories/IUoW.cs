﻿namespace KeepAlive.Domain.Repositories
{
    using Entities;

    public interface IUoW
    {
        IRepository<Website> Websites { get; }

        #region Methods

        void Commit();

        #endregion
    }
}