﻿namespace KeepAlive.Harness
{
    using System;
    using System.Timers;
    using Domain.Repositories;
    using Domain.Service;
    using Ninject;
    using Repository;
    using Service;
    using Task;

    internal class Program
    {
        private static Timer _timer;

        #region Methods

        private static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IWebsiteService>().To<WebsiteService>();
            kernel.Bind<TaskRunner>().ToSelf().InSingletonScope();
            kernel.Bind<IUoW>().To<UoW>();
            kernel.Bind<RepositoryFactories>().ToSelf().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();

            TaskRunner task = kernel.Get<TaskRunner>();

            using (_timer = new Timer(6000))
            {
                _timer.Elapsed += TimerElapsed;
                _timer.Start();
                while (true)
                {
                    if (Console.ReadLine() == "quit")
                    {
                        break;
                    }
                }
            }
        }

        private static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            TaskRunner.Run();
            _timer.Start();
        }

        #endregion
    }
}