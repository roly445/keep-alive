﻿namespace KeepAlive.Site.Models
{
    using System.ComponentModel.DataAnnotations;

    public class WebsiteModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Url { get; set; }
    }
}