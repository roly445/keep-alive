﻿namespace KeepAlive.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;
    using Domain.Service;

    public class WebsiteService : IWebsiteService
    {
        private readonly IUoW _uow;

        public WebsiteService(IUoW uow)
        {
            _uow = uow;
        }

        #region IWebsiteService Members

        public Website Create(Website entity)
        {
            _uow.Websites.Add(entity);
            _uow.Commit();

            return entity;
        }

        public IQueryable<Website> SelectAll()
        {
            return _uow.Websites.GetAll();
        }

        public Website SelectById(int id)
        {
            return _uow.Websites.GetById(id);
        }

        public void Update(Website entity)
        {
            _uow.Websites.Update(entity);
            _uow.Commit();
        }

        public void Delete(Website entity)
        {
            _uow.Websites.Delete(entity);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.Websites.Delete(id);
            _uow.Commit();
        }

        #endregion
    }
}