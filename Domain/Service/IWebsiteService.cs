﻿namespace KeepAlive.Domain.Service
{
    using Entities;

    public interface IWebsiteService : IService<Website> {}
}