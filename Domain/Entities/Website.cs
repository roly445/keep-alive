﻿namespace KeepAlive.Domain.Entities
{
    public class Website : BaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}