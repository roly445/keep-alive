﻿namespace KeepAlive.Repository
{
    using System;
    using Domain.Entities;
    using Domain.Repositories;

    public class UoW : IUoW, IDisposable
    {
        private RepoDbContext _dbContext;

        public UoW(IRepositoryProvider repositoryProvider)
        {
            CreateDbContext();
            repositoryProvider.DbContext = _dbContext;
            RepositoryProvider = repositoryProvider;
        }

        protected IRepositoryProvider RepositoryProvider { get; set; }

        #region IDisposable Members

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        #endregion

        #region IUoW Members

        public IRepository<Website> Websites { get { return GetStandardRepo<Website>(); } }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        #endregion

        #region Methods

        protected void CreateDbContext()
        {
            _dbContext = new RepoDbContext();
        }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        #endregion
    }
}