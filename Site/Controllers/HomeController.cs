﻿namespace KeepAlive.Site.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Entities;
    using Models;
    using Service;

    public class HomeController : Controller
    {
        private readonly WebsiteService _websiteService;

        public HomeController(WebsiteService websiteService)
        {
            _websiteService = websiteService;
        }

        //
        // GET: /Home/

        #region Methods

        public ActionResult Index()
        {
            if (TempData ["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData ["ViewData"];
            }

            return View(_websiteService.SelectAll().ToList());
        }

        public ActionResult NewWebsite()
        {
            return PartialView("_NewWebsite");
        }

        [HttpPost]
        public ActionResult NewWebsite(WebsiteModel model)
        {
            if (!ModelState.IsValid)
                TempData ["ViewData"] = ViewData;
            else
                _websiteService.Create(new Website {Name = model.Name, Url = model.Url});
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _websiteService.Delete(id);
            return RedirectToAction("Index");
        }

        #endregion
    }
}